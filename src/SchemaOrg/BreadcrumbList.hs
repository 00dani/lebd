{-# LANGUAGE OverloadedStrings #-}
module SchemaOrg.BreadcrumbList ( breadcrumbList ) where

import Data.Aeson
import qualified Data.Text as T

breadcrumbList :: [(a, T.Text)] -> (a -> [(T.Text, T.Text)] -> T.Text) -> Value
breadcrumbList crumbs url = object
  [ ("@context", "http://schema.org")
  , ("@type", "BreadcrumbList")
  , "itemListElement" .= zipWith (listItem url) [1 :: Int ..] crumbs
  ]
listItem :: (a -> [(T.Text, T.Text)] -> T.Text) -> Int -> (a, T.Text) -> Value
listItem url i (r, t) = object
  [ ("@type", "ListItem")
  , "position" .= i
  , "item" .= object
    [ "@id" .= url r []
    , "name" .= t
    ]
  ]
