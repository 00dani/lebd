{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
module Settings.StaticR ( staticR ) where

import Foundation ( App, Route(StaticR) )
import Settings ( appStaticDir, compileTimeAppSettings )
import Yesod.Static ( Route(StaticRoute) )

import Settings.StaticR.TH ( mkHashMap )

import qualified Data.Map as M
import qualified Data.Text as T

staticR :: [T.Text] -> Route App
staticR pieces = StaticR $ StaticRoute pieces params
    where params = case pieces `M.lookup` staticMap of
                     Just etag -> [("etag", etag)]
                     Nothing -> []

staticMap :: M.Map [T.Text] T.Text
staticMap = M.fromList $(mkHashMap . appStaticDir $ compileTimeAppSettings)
