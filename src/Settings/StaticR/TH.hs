{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE TupleSections     #-}
module Settings.StaticR.TH ( mkHashMap ) where

import Conduit hiding ( lift )
import Data.List.Split ( splitOn )
import Language.Haskell.TH.Syntax ( Q, Exp, lift, runIO )
import Yesod.Static ( base64md5 )

import qualified Data.ByteString.Lazy as L

base64md5File :: MonadIO m => FilePath -> m String
base64md5File = fmap base64md5 . liftIO . L.readFile

genHashPair :: MonadIO m => FilePath -> m ([String], String)
genHashPair fp = (tail $ splitOn "/" fp,) <$> base64md5File fp

genHashMap :: FilePath -> IO [([String], String)]
genHashMap dir = runConduitRes
               $ sourceDirectoryDeep True dir
              .| mapMC genHashPair
              .| sinkList

mkHashMap :: FilePath -> Q Exp
mkHashMap fp = lift =<< runIO (genHashMap fp)
