{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}

module Foundation where

import Import.NoFoundation
import Database.Persist.Sql (ConnectionPool, runSqlPool)
import Text.Hamlet          (hamletFile)
import Text.Jasmine         (minifym)
import Web.Slug             (Slug)

import Yesod.Auth.HashDB    (authHashDBWithForm)
import qualified Yesod.Auth.Message as AuthMsg

import Yesod.Default.Util   (addStaticContentExternal)
import Yesod.Core.Types     (Logger)
import qualified Yesod.Core.Unsafe as Unsafe

import Package
import Model.Cache ( getCached )
import Model.Category ( Category )
import Model.Entry ( entryTitle )
import Model.Entry.Kind ( EntryKind, allEntryKinds, pluralise )
import SchemaOrg.BreadcrumbList ( breadcrumbList )

import Data.Aeson ( encode )
import qualified Text.Blaze.Internal as B
import qualified Data.Text as T
import qualified Data.Text.Lazy.Encoding as E
import qualified System.Posix.Files as F

-- | The foundation datatype for your application. This can be a good place to
-- keep settings and values requiring initialization before your application
-- starts running, such as database connections. Every handler will have
-- access to the data present here.
data App = App
    { appSettings    :: AppSettings
    , appStatic      :: Static -- ^ Settings for static file serving.
    , appConnPool    :: ConnectionPool -- ^ Database connection pool.
    , appHttpManager :: Manager
    , appLogger      :: Logger
    }

data MenuItem = MenuItem
    { menuItemLabel :: Text
    , menuItemRoute :: Route App
    }

-- This is where we define all of the routes in our application. For a full
-- explanation of the syntax, please see:
-- http://www.yesodweb.com/book/routing-and-handlers
--
-- Note that this is really half the story; in Application.hs, mkYesodDispatch
-- generates the rest of the code. Please see the following documentation
-- for an explanation for this split:
-- http://www.yesodweb.com/book/scaffolding-and-the-site-template#scaffolding-and-the-site-template_foundation_and_application_modules
--
-- This function also generates the following type synonyms:
-- type Handler = HandlerT App IO
-- type Widget = WidgetT App IO ()
mkYesodData "App" $(parseRoutesFile "config/routes")

-- | A convenient synonym for creating forms.
type Form x = Html -> MForm (HandlerT App IO) (FormResult x, Widget)

sessionLifetime :: Int
sessionLifetime = 120 -- minutes

-- Please see the documentation for the Yesod typeclass. There are a number
-- of settings which can be configured by overriding methods here.
instance Yesod App where
    -- Controls the base of generated URLs. For more information on modifying,
    -- see: https://github.com/yesodweb/yesod/wiki/Overriding-approot
    approot = ApprootMaster $ fromMaybe "localhost" . appRoot . appSettings

    -- Store session data on the client in encrypted cookies,
    -- default session idle timeout is 120 minutes
    makeSessionBackend _ = sslOnlySessions . strictSameSiteSessions $ Just <$> defaultClientSessionBackend
        sessionLifetime
        "config/client_session_key.aes"

    -- Redirect static requests to a subdomain - this is recommended for best
    -- performance, since serving static files does not need your session
    -- cookies and they can be served from the frontend HTTP server without
    -- hitting the app server at all.
    -- https://github.com/yesodweb/yesod-cookbook/blob/master/cookbook/Serve-static-files-from-a-separate-domain.md
    urlParamRenderOverride app (StaticR s) _ = do
      staticRoot <- appStaticRoot . appSettings $ app
      return . uncurry (joinPath app staticRoot) . renderRoute $ s
    urlParamRenderOverride _ _ _ = Nothing

    -- Yesod Middleware allows you to run code before and after each handler function.
    -- The defaultYesodMiddleware adds the response header "Vary: Accept, Accept-Language" and performs authorization checks.
    -- Some users may also want to add the defaultCsrfMiddleware, which:
    --   a) Sets a cookie with a CSRF token in it.
    --   b) Validates that incoming write requests include that token in either a header or POST parameter.
    -- To add it, chain it together with the defaultMiddleware: yesodMiddleware = defaultYesodMiddleware . defaultCsrfMiddleware
    -- For details, see the CSRF documentation in the Yesod.Core.Handler module of the yesod-core package.
    yesodMiddleware = defaultYesodMiddleware . defaultCsrfMiddleware . sslOnlyMiddleware sessionLifetime

    defaultLayout widget = do
        master <- getYesod
        mmsg <- getMessage

        muser <- maybeAuthPair
        mcurrentRoute <- getCurrentRoute

        -- Get the breadcrumbs, as defined in the YesodBreadcrumbs instance.
        (title, crumbs) <- breadcrumbs
        let allCrumbs = maybe crumbs (\route -> crumbs ++ [(route, title)]) mcurrentRoute
        jsonCrumbs <- fmap (E.decodeUtf8 . encode) . withUrlRenderer $ breadcrumbList allCrumbs

        let navbars = [leftMenuItems, rightMenuItems] <*> [muser]

        -- We break up the default layout into two components:
        -- default-layout is the contents of the body tag, and
        -- default-layout-wrapper is the entire page. Since the final
        -- value passed to hamletToRepHtml cannot be a widget, this allows
        -- you to use normal widget features in default-layout.

        pc <- widgetToPageContent $(widgetFile "default-layout")
        let globalTitle = toHtml . siteTitle . appSettings $ master
            hasPageTitle = not . B.null $ pageTitle pc
            fullTitle = if hasPageTitle then mconcat [pageTitle pc, " ~ ", globalTitle] else globalTitle
        withUrlRenderer $(hamletFile "templates/default-layout-wrapper.hamlet")

    -- The page to be redirected to when authentication is required.
    authRoute _ = Just $ AuthR LoginR

    -- Routes not requiring authentication.
    isAuthorized _ _ = return Authorized

    -- This function creates static content files in the static folder
    -- and names them based on a hash of their content. This allows
    -- expiration dates to be set far in the future without worry of
    -- users receiving stale content.
    addStaticContent ext mime content = do
        master <- getYesod
        let staticDir = appStaticDir $ appSettings master
        external <- addStaticContentExternal
            minifym
            genFileName
            staticDir
            (StaticR . flip StaticRoute [])
            ext
            mime
            content
        case external of
          (Just (Right (StaticR (StaticRoute filePath _), _))) -> liftIO $ do
            let staticPath = ((staticDir ++ "/") ++) . T.unpack . T.intercalate "/" $ filePath
            mode <- F.fileMode <$> F.getFileStatus staticPath
            F.setFileMode staticPath $ mode `F.unionFileModes` F.groupReadMode
          _ -> return ()
        return external
      where
        -- Generate a unique filename based on the content itself
        genFileName lbs = "autogen-" ++ base64md5 lbs

    -- What messages should be logged. The following includes all messages when
    -- in development, and warnings and errors in production.
    shouldLog app _source level =
        appShouldLogAll (appSettings app)
            || level == LevelWarn
            || level == LevelError

    makeLogger = return . appLogger

leftMenuItems, rightMenuItems :: Maybe (UserId, User) -> [MenuItem]
leftMenuItems _ = toMenuItem <$> take 5 allEntryKinds
  where toMenuItem kind = pluralise kind `MenuItem` EntriesR kind
rightMenuItems = loggedOutItems `maybe` loggedInItems
  where loggedOutItems =
          [ "log in" `MenuItem` AuthR LoginR
          ]
        loggedInItems (_id, user) =
          [ userUsername user `MenuItem` HomeR
          , "log out" `MenuItem` AuthR LogoutR
          ]

-- Define breadcrumbs.
instance YesodBreadcrumbs App where
  breadcrumb (AuthR _) = return ("log in", Just HomeR)
  breadcrumb (EntriesR kind) = return (pluralise kind, Just HomeR)
  breadcrumb (EntryR kind entryId) = do
    (Entity _ entry) <- getCached entryId
    return (entryTitle entry, Just $ EntriesR kind)
  breadcrumb (EntryWithSlugR kind entryId _) = breadcrumb $ EntryR kind entryId
  breadcrumb _ = return ("home", Nothing)

-- How to run database actions.
instance YesodPersist App where
    type YesodPersistBackend App = SqlBackend
    runDB action = do
        master <- getYesod
        runSqlPool action $ appConnPool master
instance YesodPersistRunner App where
    getDBRunner = defaultGetDBRunner appConnPool

instance YesodAuth App where
    type AuthId App = UserId

    -- Where to send a user after successful login
    loginDest _ = HomeR
    -- Where to send a user after logout
    logoutDest _ = HomeR

    authenticate = fmap toResult . lookupCreds
      where
        lookupCreds = runDB . getBy . UniqueUser . credsIdent
        toResult = UserError AuthMsg.InvalidLogin `maybe` (Authenticated . entityKey)

    -- You can add other plugins like Google Email, email or OAuth here
    authPlugins _ = [authHashDBWithForm loginForm (Just . UniqueUser)]
      where loginForm :: Route App -> Widget
            loginForm action = do
              mtok <- reqToken <$> getRequest
              setTitle "log in"
              $(widgetFile "auth/login")
    authHttpManager = getHttpManager

-- | Access function to determine if a user is logged in.
isAuthenticated :: Handler AuthResult
isAuthenticated = do
    muid <- maybeAuthId
    return $ case muid of
        Nothing -> Unauthorized "You must login to access this page"
        Just _ -> Authorized

instance YesodAuthPersist App

-- This instance is required to use forms. You can modify renderMessage to
-- achieve customized and internationalized form validation messages.
instance RenderMessage App FormMessage where
    renderMessage _ _ = defaultFormMessage

-- Useful when writing code that is re-usable outside of the Handler context.
-- An example is background jobs that send email.
-- This can also be useful for writing code that works across multiple Yesod applications.
instance HasHttpManager App where
    getHttpManager = appHttpManager

unsafeHandler :: App -> Handler a -> IO a
unsafeHandler = Unsafe.fakeHandlerGetLogger appLogger

-- Note: Some functionality previously present in the scaffolding has been
-- moved to documentation in the Wiki. Following are some hopefully helpful
-- links:
--
-- https://github.com/yesodweb/yesod/wiki/Sending-email
-- https://github.com/yesodweb/yesod/wiki/Serve-static-files-from-a-separate-domain
-- https://github.com/yesodweb/yesod/wiki/i18n-messages-in-the-scaffolding
userProfile :: User -> Route App
userProfile user
  | userUsername user == siteUsername compileTimeAppSettings = HomeR
  | otherwise = error "Multiple profile pages are not yet supported"
