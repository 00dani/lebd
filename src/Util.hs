module Util ( compileMustache, entityToTuple, mapFirst ) where

import Database.Persist ( Entity(..), Key )
import Text.Mustache ( Template(..), compileTemplate )
import Text.Mustache.Types ( Node(TextBlock) )
import Text.Parsec.Error ( ParseError )

import qualified Data.Text as T
import qualified Data.HashMap.Lazy as M

mapFirst :: (a -> a) -> [a] -> [a]
mapFirst f (x:xs) = f x : xs
mapFirst _ [] = []

compileMustache :: String -> T.Text -> Template
compileMustache n = either errorTemplate id . compileTemplate n

errorTemplate :: ParseError -> Template
errorTemplate err = Template
    { name = "error"
    , ast = [TextBlock . T.pack $ show err]
    , partials = M.empty
    }

entityToTuple :: Entity t -> (Key t, t)
entityToTuple (Entity key value) = (key, value)
