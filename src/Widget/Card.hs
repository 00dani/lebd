{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
module Widget.Card ( hCard ) where

import Import

import Database.Esqueleto ( (^.) )
import qualified Database.Esqueleto as E

import Model.Markdown ( unMarkdown )
import Text.Mustache ( substitute )
import Util ( compileMustache )

import qualified Data.Text as T

prettyPgp :: PgpKey -> Text
prettyPgp = T.unwords . T.chunksOf 4 . pgpKeyFingerprint

routeFromPgp :: PgpKey -> Route App
routeFromPgp PgpKey { pgpKeyFingerprint = f } = staticR ["pgp", T.takeEnd 8 f ++ ".asc"]

profileUrl :: Site -> Profile -> Text
profileUrl site = substitute $ T.unpack (siteName site) `compileMustache` siteTemplate site

hCard :: Entity User -> Widget
hCard (Entity userId user) = do
    let (firstName:lastName) = T.words $ userFullName user
    mcurrentRoute <- getCurrentRoute
    userProfiles <- handlerToWidget . runDB . E.select . E.from $ \(profile `E.InnerJoin` site) -> do
        E.on $ profile ^. ProfileSiteId E.==. site ^. SiteId
        E.where_ $ profile ^. ProfileUserId E.==. E.val userId
        E.orderBy [E.asc $ site ^. SiteName]
        return (site, profile)
    pgpKeys <- handlerToWidget . runDB $ map entityVal <$> selectList [PgpKeyUserId ==. userId] []
    let maybeFb = find (\(Entity _ site, _) -> "Facebook" == siteName site) userProfiles

    toWidgetHead [hamlet|
        <meta name="author" content=#{userFullName user}>
        <meta name="description" content=#{unMarkdown $ userNote user}>
        <link rel="author" href=@{HomeR}>
        <meta property="og:type" content="profile">
        <meta property="og:title" content="#{userFullName user}">
        <meta property="og:description" content=#{unMarkdown $ userNote user}>
        <meta property="og:image" content=@{staticR ["img", userAvatar user]}>
        <meta property="profile:first_name" content=#{firstName}>
        <meta property="profile:last_name" content=#{T.unwords lastName}>
        <meta property="profile:username" content=#{userUsername user}>
        $maybe (_, Entity _ fb) <- maybeFb
            <meta property="fb:profile_id" content=#{profileUsername fb}>
        $forall key <- pgpKeys
            <link rel="pgpkey" type="application/pgp-keys" href=@{routeFromPgp key}>
    |]

    $(widgetFile "mf2/h-card")
