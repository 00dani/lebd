{-# LANGUAGE TemplateHaskell #-}
module Widget.Feed ( hFeed ) where

import Import
import Widget.Entry ( hEntry )

import qualified Data.Text as T

hFeed :: T.Text -> [Entity Entry] -> Widget
hFeed name entries = do
    mroute <- getCurrentRoute
    $(widgetFile "mf2/h-feed")
