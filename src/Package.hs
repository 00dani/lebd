{-# LANGUAGE TemplateHaskell #-}

module Package ( Package(..)
               , Repository(..)
               , package
               ) where

import Data.Aeson ( eitherDecodeStrict )
import Data.Either ( either )
import Language.Haskell.TH.Syntax ( addDependentFile, lift, runIO )
import Package.Types

import qualified Data.ByteString as B

package :: Package
package = $(do
    let f = "package.json"
    addDependentFile f
    json <- runIO $ B.readFile f
    let result = eitherDecodeStrict json :: Either String Package
    either fail lift result)
