{-# LANGUAGE DeriveLift #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}

module Package.Types where

import Data.Aeson
import Data.Aeson.Types ( fieldLabelModifier )
import Data.Aeson.TH ( deriveJSON )

import Data.Char ( toLower )
import Language.Haskell.TH.Syntax ( Lift )
import Util ( mapFirst )

data Package = Package
    { packageName :: !String
    , packageVersion :: !String
    , packageRepository :: !Repository
    } deriving (Show, Lift)

data Repository = Repository
    { repositoryType :: !String
    , repositoryUrl :: !String
    } deriving (Show, Lift)

$(deriveJSON defaultOptions { fieldLabelModifier = mapFirst toLower . drop 7 } ''Package)
$(deriveJSON defaultOptions { fieldLabelModifier = mapFirst toLower . drop 10 } ''Repository)
