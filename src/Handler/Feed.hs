{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Handler.Feed where

import Import

import Data.Time.Clock.POSIX ( posixSecondsToUTCTime )
import Model.Entry ( entryTitle )
import Widget.Entry ( entryR )

import qualified Data.Text as T
import qualified Model.Entry.Kind as K

getFeedR :: Handler TypedContent
getFeedR = do
    entries <- runDB $ selectList [] [Desc EntryPublished]
    newsFeed $ toFeed entries

getFeedKindR :: K.EntryKind -> Handler TypedContent
getFeedKindR kind = do
    entries <- runDB $ selectList [EntryKind ==. kind] [Desc EntryPublished]
    let basicFeed = toFeed entries
    newsFeed $ basicFeed
        { feedTitle = T.concat [K.pluralise kind, " ~ ", feedTitle basicFeed]
        , feedDescription = toHtml $ T.concat ["feed of all ", K.pluralise kind]
        , feedLinkSelf = FeedKindR kind
        , feedLinkHome = EntriesR kind
        }

toFeed :: [Entity Entry] -> Feed (Route App)
toFeed entries@(latestEntry:_) = (toFeed [])
    { feedEntries = toFeedEntry <$> entries
    , feedUpdated = entryUpdated $ entityVal latestEntry
    }
toFeed [] = Feed
    { feedTitle = siteTitle compileTimeAppSettings
    , feedLinkSelf = FeedR
    , feedLinkHome = HomeR
    , feedAuthor = ""
    , feedDescription = "sitewide feed of all entries"
    , feedLanguage = "en-au"
    , feedUpdated = posixSecondsToUTCTime 0
    , feedLogo = Nothing
    , feedEntries = []
    }

toFeedEntry :: Entity Entry -> FeedEntry (Route App)
toFeedEntry entry = FeedEntry
    { feedEntryLink = entryR entry
    , feedEntryUpdated = entryUpdated $ entityVal entry
    , feedEntryTitle = entryTitle $ entityVal entry
    , feedEntryContent = toHtml . entryContent . entityVal $ entry
    , feedEntryEnclosure = Nothing
    }
