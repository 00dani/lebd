{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
module Handler.Home where

import Import

import Yesod.AtomFeed ( atomLink )

import Widget.Card ( hCard )
import Widget.Feed ( hFeed )

getHomeR :: Handler Html
getHomeR = do
    settings <- asks appSettings
    user <- runDB . getBy404 . UniqueUser . siteUsername $ settings
    let title = siteTitle settings
    entries <- runDB $ selectList [EntryAuthorId ==. entityKey user] [Desc EntryPublished]
    defaultLayout $ do
        atomLink FeedR title
        $(widgetFile "home")
