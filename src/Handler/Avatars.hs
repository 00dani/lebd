{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Handler.Avatars where

import Import hiding ( (==.) )

import Database.Esqueleto
import Settings.StaticR ( staticR ) 

getAvatarR :: UserId -> Handler ()
getAvatarR = responseFrom <=< runDB . select . from . queryAvatar
    where responseFrom (a:_) = redirect $ staticR ["img", unValue a]
          responseFrom    [] = notFound
          queryAvatar userId user = do
              where_ $ user ^. UserId ==. val userId
              return $ user ^. UserAvatar
