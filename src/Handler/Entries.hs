{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}
module Handler.Entries where

import Import

import Yesod.AtomFeed ( atomLink )

import Model.Cache ( getCached )
import Model.Entry ( entryTitle )
import Model.Markdown ( unMarkdown )
import Widget.Entry ( entryR, hEntry )
import Widget.Feed ( hFeed )

import qualified Data.Text as T
import qualified Model.Entry.Kind as K

getEntriesR :: K.EntryKind -> Handler Html
getEntriesR kind = do
    entries <- runDB $ selectList [EntryKind ==. kind] [Desc EntryPublished]
    title <- asks $ siteTitle . appSettings
    let myTitle = T.concat [K.pluralise kind, " ~ ", title]
    defaultLayout $ do
        setTitle . toHtml . K.pluralise $ kind
        FeedKindR kind `atomLink` myTitle
        hFeed myTitle entries

getEntryR :: a -> EntryId -> Handler Html
getEntryR _ = renderEntry <=< getCached

getEntryWithSlugR :: a -> EntryId -> b -> Handler Html
getEntryWithSlugR kind = const . getEntryR kind

renderEntry :: (Entity Entry) -> Handler Html
renderEntry entry = do
    let correctRoute = entryR entry
    actualRoute <- getCurrentRoute
    author <- getCached . entryAuthorId $ entityVal entry
    when (actualRoute /= Just correctRoute) $
        redirectWith movedPermanently301 correctRoute
    defaultLayout $ do
        setTitle . toHtml . entryTitle . entityVal $ entry
        toWidgetHead [hamlet|
            <meta name="author" content=#{userFullName $ entityVal author}>
            <link rel="author" href=@{userProfile $ entityVal author}>
            <meta name="description" content=#{unMarkdown $ entryContent $ entityVal entry}>
            <meta property="og:title" content=#{entryTitle $ entityVal entry}>
            <meta property="og:type" content="article">
            <meta property="og:description" content=#{unMarkdown $ entryContent $ entityVal entry}>
            <meta property="article:author" content=@{userProfile $ entityVal author}>
            <meta property="article:section" content=#{K.pluralise $ entryKind $ entityVal entry}>
            |]
        hEntry entry
