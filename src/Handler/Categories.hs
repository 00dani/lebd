{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Handler.Categories where

import Import hiding ( on, (==.) )

import Database.Esqueleto
import Widget.Feed ( hFeed )
import Model.Category ( Category, asTag )

import qualified Data.Text as T

getCategoryR :: Category -> Handler Html
getCategoryR tag = do
    title <- asks $ siteTitle . appSettings
    entries <- runDB . select . from $ \(entry `InnerJoin` category) -> do
        on $ entry ^. EntryId ==. category ^. EntryCategoryEntryId
        where_ $ category ^. EntryCategoryCategory ==. val tag
        return entry
    defaultLayout $ do
        setTitle . toHtml . asTag $ tag
        T.concat [asTag tag, " ~ ", title] `hFeed` entries
