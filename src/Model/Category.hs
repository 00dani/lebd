{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Model.Category where

import Database.Persist ( PersistField )
import Web.Slug ( Slug, unSlug )
import Yesod ( PathPiece )

import qualified Data.Text as T

newtype Category = Category { unCategory :: Slug }
    deriving (Eq, Read, Show, PathPiece, PersistField)

asTag :: Category -> T.Text
asTag = T.cons '#' . unSlug . unCategory
