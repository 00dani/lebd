{-# LANGUAGE OverloadedStrings #-}
module Model.Entry where

import Model ( Entry, entryName, entryContent )
import Model.Markdown ( Markdown(Markdown), unMarkdown )
import Data.Maybe ( fromMaybe )

import qualified Data.Text as T
import qualified Data.Text.Lazy as TL

entryTitle :: Entry -> T.Text
entryTitle = fromMaybe <$> TL.toStrict . unMarkdown . shorten 30 . entryContent <*> entryName

class Shorten a where
  shorten :: Int -> a -> a

instance Shorten T.Text where
  shorten i t
    | T.compareLength t n == GT = flip T.append "..." . T.take (n - 1) $ t
    | otherwise                 = t
    where n = fromIntegral i

instance Shorten TL.Text where
  shorten i t
    | TL.compareLength t n == GT = flip TL.append "..." . TL.take (n - 1) $ t
    | otherwise                  = t
    where n = fromIntegral i

instance Shorten Markdown where
  shorten n (Markdown t) = Markdown $ shorten n t
