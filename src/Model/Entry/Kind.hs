{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
module Model.Entry.Kind where

import Database.Persist.TH ( derivePersistField )
import Yesod.Core.Dispatch ( PathPiece, toPathPiece, fromPathPiece )

import qualified Data.Text as T
import Text.Read ( readMaybe )

data EntryKind = Note | Article | Photo | Video | Audio
               | Reply | Repost | Like | Favourite | Bookmark | Quote | Rsvp
               | Listen | Jam | Watch | Play | Read
              deriving (Enum, Eq, Read, Show)
derivePersistField "EntryKind"

allEntryKinds :: [EntryKind]
allEntryKinds = [Note ..]

singularise :: T.Text -> Maybe EntryKind
singularise "replies" = Just Reply
singularise "watches" = Just Watch
singularise k = readMaybe . T.unpack . T.toTitle . T.init $ k

pluralise :: EntryKind -> T.Text
pluralise Reply = "replies"
pluralise Watch = "watches"
pluralise k = T.toLower . flip T.snoc 's' . T.pack . show $ k

instance PathPiece EntryKind where
    toPathPiece = pluralise
    fromPathPiece = singularise
