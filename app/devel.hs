{-# LANGUAGE PackageImports #-}
import "lebd" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
